---
swagger: "2.0"
info:
  description: Api encargada de la evaluacion para la transaccion
  version: v1
  title: API Visanet Antifraud
  termsOfService: http://www.visanet.com.pe
  contact:
    name: VisaNet
    url: http://www.visanet.com.pe
    email: visanet@gmail.com
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
x-samples-languages:
- 'curl'
- 'csharp'
- 'php'
- 'java'
- 'node'
host: apitestenv.vnforapps.com
basePath: /api.antifraud/v1
tags:
- name: Antifraud Api
  description: RESTful API to evaluate transactions.
schemes:
- https
paths:
  /antifraud/{product}/{merchantId}:
    post:
      tags:
      - Antifraud Api
      summary: Antifraud
      description: RESTful API to evaluate transactions.
      operationId: evaluate
      consumes:
      - application/json; charset=utf-8
      produces:
      - application/json; charset=utf-8
      parameters:
      - name: product
        in: path
        description: Identificador del producto
        required: true
        type: string
        x-example: ecommerce
        enum:
        - recurrence
        - ecommerce
      - name: merchantId
        in: path
        description: Identificador del comercio
        required: true
        type: string
        x-example: "341198210"
      - in: body
        name: body
        description: Antifraud request
        required: true
        schema:
          $ref: '#/definitions/AntifraudRequest'
      responses:
        200:
          description: successful operation
          schema:
            $ref: '#/definitions/AntifraudResponse'
        401:
          description: Unevaluated
        500:
          description: Request is not valid
      security:
      - accessToken: []
securityDefinitions:
  accessToken:
    description: Access token obtained from api.security
    type: apiKey
    name: Authorization
    in: header
definitions:
  Address:
    type: object
    properties:
      street1:
        type: string
        example: Calle Astorga 827
        description: Direccion
      street2:
        type: string
        example: Calle Astorga 831
        description: Direccion
      postalCode:
        type: string
        example: Lima 30
        description: Codigo Postal
      city:
        type: string
        example: Lima
        description: Ciudad
      state:
        type: string
        example: LI
        description: Departamento / Estado
      country:
        type: string
        example: PE
        description: Pais
    description: Entidad que representa informacion de direccion
    example:
      country: PE
      city: Lima
      postalCode: Lima 30
      street1: Calle Astorga 827
      street2: Calle Astorga 831
      state: LI
  AntifraudRequest:
    type: object
    required:
    - billingAddress
    - card
    - cardHolder
    - channel
    - clientIp
    - deviceFingerprintId
    - merchantDefineData
    - order
    - shippingAddress
    properties:
      channel:
        type: string
        example: web
        description: Canal desde donde se origino la transaccion
        enum:
        - mobile
        - web
        - mpos
        - host
        - pos
        - kiosk
        - callcenter
        - pasarela
      clientIp:
        type: string
        example: 24.212.107.30
        description: Direccion IP del cliente
      deviceFingerprintId:
        type: string
        description: Device Finger Print Id
      merchantDefineData:
        type: object
        description: Entidad que representa a datos adicionales definidos por el comercio
        additionalProperties:
          type: string
      billingAddress:
        description: Entidad que representa a la direccion de facturacion
        $ref: '#/definitions/Address'
      shippingAddress:
        description: Entidad que representa a la direccion de envio
        $ref: '#/definitions/Address'
      order:
        description: Entidad que representa el pedido
        $ref: '#/definitions/Order'
      card:
        description: Entidad que representa al tarjetahabiente
        $ref: '#/definitions/Card'
      cardHolder:
        description: Entidad que representa a los datos del tarjetahabiente
        $ref: '#/definitions/CardHolder'
      dataMap:
        type: object
        description: Entidad que representa informacion complementaria de la transaccion
        additionalProperties:
          type: string
      trip:
        description: Entidad que representa la informacion del viaje
        $ref: '#/definitions/Trip'
      passengers:
        type: array
        description: Lista que representa informacion de los pasajeros
        items:
          description: Entidad que representa informacion del pasajero
          $ref: '#/definitions/Passenger'
    description: Antifraud transaction request
    example:
      channel: web
      cardHolder:
        firstName: Pedro
        lastName: Galdamez
        phoneNumber: +51 1 2223333
        email: info@quiputech.com
      captureType: manual
      dataMap:
        key: dataMap
      card:
        expirationYear: 2029
        cvv2: 329
        expirationMonth: 12
        cardNumber: 4551478422045511
      order:
        amount: 1
        currency: PEN
        purchaseNumber: 1234567890
      clientIp: 24.212.107.30
      deviceFingerprintId: deviceFingerprintId
      shippingAddress:
        country: PE
        city: Lima
        postalCode: Lima 30
        street1: Calle Astorga 827
        street2: Calle Astorga 831
        state: LI
      billingAddress:
        country: PE
        city: Lima
        postalCode: Lima 30
        street1: Calle Astorga 827
        street2: Calle Astorga 831
        state: LI
      merchantDefineData:
        key: merchantDefineData
  AntifraudResponse:
    type: object
    required:
    - dataMap
    - header
    - order
    - token
    properties:
      header:
        description: Entidad que representa el resultado de la transaccion
        $ref: '#/definitions/Header'
      order:
        description: Entidad que representa el pedido
        $ref: '#/definitions/Order'
      token:
        description: Entidad que representa la creacion del token para autorizacion
        $ref: '#/definitions/TransactionToken'
      dataMap:
        $ref: '#/definitions/AntifraudResponse_dataMap'
    example:
      dataMap:
        TRANSACTION_DATE: 180711110529
        MESSAGE: Cybersource Accept
        REQUEST_TOKEN: AhjzbwSTIAgshHJItMtMIAFReTLomuEuDPD4ABeDSTLF18DBQQOwX3ZL
        DECISION: Estado de la transaccion
        ACTION_CODE: 0
        REASON_CODE: 100
        REQUEST_ID: 5.31325128450601E21
      header:
        ecoreTransactionDate: 2000-01-23T04:56:07.000Z
        millis: 0
        ecoreTransactionUUID: ecoreTransactionUUID
      order:
        amount: 1
        currency: PEN
        purchaseNumber: 1234567890
      token:
        tokenId: 1DBA413A2CAF43B5BA413A2CAFA3B5CD
        expiration: 1531322908866
        redirectToVbV: false
  Card:
    type: object
    properties:
      cardNumber:
        type: string
        example: "4551478422045511"
        description: Numero de tarjeta
      expirationMonth:
        type: integer
        format: int32
        example: 12
        description: Mes de expiracion
      expirationYear:
        type: integer
        format: int32
        example: 2029
        description: Año de expiracion
      cvv2:
        type: string
        example: "329"
        description: CVV2
    description: Entidad que representa informacion de la tarjeta
    example:
      expirationYear: 2029
      cvv2: 329
      expirationMonth: 12
      cardNumber: 4551478422045511
  CardHolder:
    type: object
    properties:
      firstName:
        type: string
        example: Pedro
        description: Nombre
      lastName:
        type: string
        example: Galdamez
        description: Apellido
      email:
        type: string
        example: info@quiputech.com
        description: Correo electronico
      phoneNumber:
        type: string
        example: +51 1 2223333
        description: Telefono
    description: Entidad que representa la informacion del tarjetahabiente
    example:
      firstName: Pedro
      lastName: Galdamez
      phoneNumber: +51 1 2223333
      email: info@quiputech.com
  Header:
    type: object
    properties:
      ecoreTransactionUUID:
        type: string
        description: Identificador unico de transaccion para ecore
      ecoreTransactionDate:
        type: string
        format: date-time
        description: Fecha de la operacion
      millis:
        type: integer
        format: int32
        description: Tiempo de ejecucion
    description: Encabezado de la transaccion
    example:
      ecoreTransactionDate: 2000-01-23T04:56:07.000Z
      millis: 0
      ecoreTransactionUUID: ecoreTransactionUUID
  Order:
    type: object
    required:
    - amount
    - currency
    - purchaseNumber
    properties:
      purchaseNumber:
        type: string
        example: "1234567890"
        description: Numero del pedido
      amount:
        type: number
        format: double
        example: 1.0
        description: Importe del pedido
      currency:
        type: string
        example: PEN
        description: Moneda
        enum:
        - PEN
        - USD
    description: Entidad que representa la informacion del pedido
    example:
      amount: 1
      currency: PEN
      purchaseNumber: 1234567890
  TransactionToken:
    type: object
    required:
    - expiration
    - redirectToVbV
    - tokenId
    properties:
      tokenId:
        type: string
        example: 1DBA413A2CAF43B5BA413A2CAFA3B5CD
        description: Id del token de transaccion obtenido en la respuesta al proceso antifraude y usado para la autorizacion
      expiration:
        type: integer
        format: int64
        example: 1531322908866
        description: Fecha de expiracion del token de transaccion
      redirectToVbV:
        type: boolean
        example: false
        description: Indicador de participacion en verified by visa
    description: Respuesta de creacion de token para autorizacion
    example:
      tokenId: 1DBA413A2CAF43B5BA413A2CAFA3B5CD
      expiration: 1531322908866
      redirectToVbV: false
  Trip:
    type: object
    required:
    - depart
    - route
    - type
    properties:
      route:
        type: string
        example: SFO-JFK:JFK-LHR:LHR-CDG
        description: Concatenacion de tramos individuales de viaje. Formato ==> ORIG1-DEST1[:ORIG2-DEST2...:ORIGn-DESTn]
      type:
        type: string
        example: round trip
        description: Tipo de viaje
      depart:
        type: string
        example: 2014-12-01 13:15 GMT-05:00
        description: Fecha y hora. Formato ==> YYYY-MM-DD HH:MM GTM HH:MM
    description: Entidad que representa la informacion del viaje
    example:
      route: SFO-JFK:JFK-LHR:LHR-CDG
      type: round trip
      depart: 2014-12-01 13:15 GMT-05:00
  Passenger:
    type: object
    required:
    - code
    - email
    - firstname
    - lastname
    - phone
    - price
    - product
    - sku
    - status
    - type
    properties:
      sku:
        type: string
        example: "341198210"
        description: Codigo de comercio
      product:
        type: string
        example: Juan Perez
        description: Nombre completo del pasajero (Nombre+Apellido)
      email:
        type: string
        example: jperez@latinmail.com
        description: Correo del pasajero
      firstname:
        type: string
        example: Juan
        description: Nombre del pasajero
      lastname:
        type: string
        example: Perez
        description: Apellido del pasajero
      code:
        type: string
        example: "42058214"
        description: Numero de documento (Pasaporte, DNI, etc)
      phone:
        type: string
        example: +51 1 2223333
        description: Telefono del pasajero
      status:
        type: string
        example: Gold
        description: Standard, Gold, or Platinum
      type:
        type: string
        example: ADT
        description: Tipo de pasajero
        enum:
        - ADT (Adult)
        - CNN (Child)
        - INF (Infant)
        - YTH (Youth)
        - STU (Student)
        - SCR (Senior Citizen)
        - MIL (Military)
      price:
        type: number
        format: double
        example: 25.0
        description: Cantidad total de la transaccion
    description: Entidad que representa informacion de los pasajeros
    example:
      sku: JFK1254
      product: Juan Perez
      email: jperez@latinmail.com
      firstname: Juan
      lastname: Perez
      code: DNI
      phone: +51 1 2223333
      status: Gold
      type: ADT
      price: 25
  AntifraudResponse_dataMap:
    properties:
      TRANSACTION_DATE:
        type: string
        example: "180711110529"
        description: Fecha de la transaccion
      MESSAGE:
        type: string
        example: Cybersource Accept
        description: Mensaje de la transaccion
      REQUEST_TOKEN:
        type: string
        example: AhjzbwSTIAgshHJItMtMIAFReTLomuEuDPD4ABeDSTLF18DBQQOwX3ZL
        description: Token de antifraude
      DECISION:
        type: string
        example: Accept
        description: Estado de la transaccion
      ACTION_CODE:
        type: string
        example: "0"
        description: Codigo de accion
      REASON_CODE:
        type: string
        example: "100"
        description: Codigo de razon
      REQUEST_ID:
        type: string
        example: 5.31325128450601E21
        description: Id de solicitud
    description: Entidad que representa informacion complementaria de la operacion
